<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReviewSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:review-summary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $reviews = json_decode(file_get_contents(public_path('datasource/reviews.json')));
        $products = json_decode(file_get_contents(public_path('datasource/products.json')));
        
        $totalReview = 0;
        $averageRatings = 0;
        $fiveStar = 0;
        $fourStar = 0;
        $threeStar = 0;
        $twoStar = 0;
        $oneStar = 0;

        $arrayRating = [];
        foreach($reviews as $review) {
            $totalReview += 1;
            $arrayRating[] = $review->rating;
            
            if($review->rating == 5) {
                $fiveStar += 1;
            }

            if($review->rating == 4) {
                $fourStar += 1;
            }

            if($review->rating == 3) {
                $threeStar += 1;
            }

            if($review->rating == 2) {
                $twoStar += 1;
            }

            if($review->rating == 1) {
                $oneStar += 1;
            }
        } 

        $averageRatings = array_sum($arrayRating) / count($arrayRating);

        $result = [
            'total_reviews' => $totalReview,
            'average_ratings' => number_format($averageRatings, 1),
            '5_star' => $fiveStar,
            '4_star' => $fourStar,
            '3_star' => $threeStar,
            '2_star' => $twoStar,
            '1_star' => $oneStar,
        ];
        
        $this->info(json_encode($result));
    }
}
